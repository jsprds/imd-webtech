var express = require('express'),
	app = express(),
	http = require('http').Server(app),
	mongoose = require('mongoose'),
	swig = require('swig'),
	bodyParser = require('body-parser'),
	path = require('path'),
	restful = require('node-restful')
	io = require('socket.io')(http);

mongoose.connect('mongodb://localhost/imd');

app.engine('html', swig.renderFile);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res) {
	res.sendFile(__dirname + '/views/index.html');
});

var Product = app.product = restful.model('Product', mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	amount: {
		type: Number,
		required: true,
		default: 0
	}
}, {collection: 'product'})).methods(['get', 'post', 'put', 'delete']);
Product.register(app, '/products');

io.on('connection', function(socket) {
	console.log('user connected');

	socket.on('test', function(msg){
	    console.log('message: ' + msg);
	});

	socket.on('updateAmount', function(data){
		console.log('UpdateAmount');
	    
	    console.log(data);

		Product.findOneAndUpdate({ _id: data.id }, { $inc: { amount: data.operator }}, {}, function(err, data) {
			console.log(err);
			console.log(data);

			io.emit('updateCallback', data);
		});
	});

	socket.on('addProduct', function(data){
		console.log('addProduct');
	    
	    console.log(data);

	    var p = new Product({name: data});

		p.save(function(err, p) {
			console.log(err);
			console.log(p);

			io.emit('addProductCallback', p);
		});
	});

});

io.on('connect', function(socket) {
	console.log('connect!');
	Product.find()
		.exec(function(err, products) {
			console.log(err);
			socket.emit('initProducts', products);
		});
});

var server = http.listen(3003, function(){
	console.log('Server running on http://localhost:3003');
});

module.exports = app;