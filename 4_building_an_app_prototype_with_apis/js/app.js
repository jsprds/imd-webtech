(function () {
    'use strict';
    /*global $, jQuery, console, localStorage, JSON */

    var WeatherApp, App;

    WeatherApp = function () {

        // functie de de locatie ophaalt van de browser --> returns lat en long
        var getLocation = function (callback) {
            navigator.geolocation.getCurrentPosition(callback);
        };

        // Functie die "menselijke data" ophaalt bij de google maps api
        var getLocationData = function (location, callback) {

            var service = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + location.coords.latitude + ',' + location.coords.longitude + '&key=AIzaSyCxu51hlWG6dB0S8ZxbYpBsmc8_C_Bnc4o';
                
            $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: service
            }).done(function (res) {

                localStorage.setItem('location', JSON.stringify(res));
                callback(res);

            }).fail(function (error) {

                console.log(error);

            }).always(function () {

                console.log('Stop loading');

            });
        }

        // functie die weather data ophaalt bij de forecast.io api
        var getWeatherFromApi = function (location, callback) {

            var service = 'https://api.forecast.io/forecast/bf8baadc6aac810515474c4762875de7/' + location.coords.latitude + ',' + location.coords.longitude;

            $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: service
            }).done(function (res) {

                localStorage.setItem('weather', JSON.stringify(res));
                callback(res);

            }).fail(function (error) {

                console.log(error);

            }).always(function () {
                
                console.log('Stop loading');

            });

        };

        // Functie de F naar C convert
        var convertToCelcius = function (value) {
            return Math.round((value -32) * 5 / 9);
        }

        // Rendert alle weather output
        var render = function (weather) {

            var renderIcon, renderCurrentWeather, renderFourDayForecast;

            // herbruikbare functie voor het renderen van icons --> dit kan nog beter als de css classes beter worden afgestemd
            renderIcon = function (e, icon) {
                switch(icon) {
                    case 'clear-day':
                        e.addClass('wi-day-sunny');
                        break;
                    case 'clear-night':
                        e.addClass('wi-night-clear');
                        break;
                    case 'rain':
                        e.addClass('wi-showers');
                        break;
                    case 'snow':
                        e.addClass('wi-snow');
                        break;
                    case 'sleet':
                        e.addClass('wi-rain-mix');
                        break;
                    case 'wind':
                        e.addClass('wi-day-windy');
                        break;
                    case 'fog':
                        e.addClass('wi-fog');
                        break;
                    case 'cloudy':
                        e.addClass('wi-day-cloudy');
                        break;
                    case 'partly-cloudy-day':
                        e.addClass('wi-day-sunny-overcast');
                        break;
                    case 'partly-cloudy-night':
                        e.addClass('wi-night-cloudy');
                        break;
                    default:
                        $('#currentIcon').addClass('wi-refresh');
                }
            }

            // current overzicht
            renderCurrentWeather = function (weather) {
                renderIcon($('#currentIcon'), weather.currently.icon); // renders big icon
                $('#currentTemp').text(convertToCelcius(weather.currently.temperature));
                $('#currentDescription').text(weather.currently.summary);
            }

            // Render een overzicht voor de volgende vier dagen
            renderFourDayForecast = function (weather) {

                var weekDays;

                // Array voor weekdagen
                weekDays = [
                    'Sunday',
                    'Monday',
                    'Tuesday',
                    'Wednesday',
                    'Thursday',
                    'Friday',
                    'Saturday'
                ];

                // clear ul --> we doen een refresh dus eerst alles weg
                $("#forcast").empty(); 

                // loop door de volgende dagen --> TO DO: starten vanaf de volgende dag
                for(var i = 0; i < (weather.daily.data.length > 4 ? 4 : weather.daily.data.length); i++) {

                    // dit is de data waar we met gaan werken voor dit blokje van de array
                    var w = weather.daily.data[i];

                    // Format date
                    var date = '<div class="weakDay">' + weekDays[(new Date(w.time * 1000)).getDay()] + '</div>';

                    // Format temperature
                    var temperatureMin = '<div class="temperatureMin">' + convertToCelcius(w.apparentTemperatureMin) + '</div>';
                    var temperatureMax = '<div class="temperatureMax">' + convertToCelcius(w.apparentTemperatureMax) + '</div>';
                    var temperature = '<div class="temperature">' + temperatureMin + temperatureMax + '</div>';

                    // icon
                    var iconDiv = $('<div class="wi"></div>');
                    renderIcon(iconDiv, w.icon);
                    console.log(iconDiv)

                    // append alles
                    $("#forcast").append('<li>' + date + temperature + iconDiv.prop('outerHTML') + '</li>');
                }
            }     

            // execute
            renderCurrentWeather(weather);
            renderFourDayForecast(weather);
        };

        // Rendert locatie data naar scherm
        var renderLocation = function (location) {
            $('#location').text(location.results[0].address_components[1].long_name + ' ' + location.results[0].address_components[2].long_name);
        };

        this.getWeather = function () {

            // Kijk eerst of we al geen data hebben in local storage
            if(localStorage.getItem('weather')) {

                // Parse data naar json
                var weather = JSON.parse(localStorage.getItem('weather'));

                // render data
                render(weather);

                // Kijk of de menselijke locatie al in de storage zit
                if(localStorage.getItem('location')) {

                    // parse data
                    var location = JSON.parse(localStorage.getItem('location'));
                    renderLocation(location);

                } else {

                    // Locatie zit niet in Storage --> vraag positie aan browser
                    getLocation(function (location) {

                        // Vraag menselijke data
                        getLocationData(location, function(res) {

                            // render de data
                            renderLocation(res);

                        });
                    });
                }
            } else {

                // we moeten alles refreshen want we hebben geen data!
                this.refreshWeather();    

            }
        }

        // Functie van "weerapp" die ervoor zorgt dat de data hernieuwt kan worden.
        this.refreshWeather = function () {

            console.log('Get from api');

            // Stap 1 --> nieuwe locatie vragen
            getLocation(function (location) {

                // Stap 2.1 --> "Menselijke data" ophalen voor deze locatie
                getLocationData(location, function(res) {

                    // Stap 3.1 --> resultaat renderen
                    renderLocation(res);

                });

                // Stap 2.2 --> Weer ophalen a.d.h.v. locatie
                getWeatherFromApi(location, function(weather) {

                    // Stap 3.1 --> resultaat renderen
                    render(weather);

                });
            });
        };
    };

    // application logic
    App = new WeatherApp();
    App.getWeather();

    // Pull to refresh action
    $('body').xpull({
        'callback':function() {
            App.refreshWeather();
        }
    });
}());