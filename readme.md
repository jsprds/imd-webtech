# Webtech2 #
your own private GitHub repository containing all your labs

* README.md in markdown describing your labs every week and
what you learned from them
* update your portfolio URL via a pull request to https://github.com/
iamgoodbytes/2imd-webtech2-labs (change your URL to your
portfolio repository)
* in a few weeks, I will pick out 2 random! labs to see if and how you
completed them (correct commits? not just one big commit?)

## Les1: GIT FTW ##
Git is super tof en cool maar je moet je hoofd erbij houden! :-)

Wat ik zelf belangrijk vind / extra op moeten letten:

* Master clean houden --> Enkel code die live kan gaan
* Voldoende tussenstappen nemen tijdens het comitten, geen grote blokken in 1x.
* Overzicht houden in de branches, eerst nadenken dan pas doen

Tijdens project PHP ben ik een interessante manier van werken tegen gekomen die ik in de toekomst wil gebruiken. Er bestaan 2 branches in de repo die ten allen tijde blijven bestaan: `master` en `develop`. De `master` moet altijd clean blijven, er mag alleen code inkomen die live mag gaan. De `develop` is een merge branch waar alle feature branches in gemerged worden tot er een `release` ontstaat die in de master gemerged kan worden. `features` worden van de `develop` branch gestart en komen terug naar de `develop` branch. Er kan met meerdere mensen gewerkt worden op een `feature` dus ook die branches mogen mee in de repo. Na het mergen van de `feature` in de `develop` kan de desbetreffende `feature` verwijdert worden. 

**Conclusie:** ik vind dit persoonelijk een heel goed systeem maar ben mij wel bewust dat het in kleine projecten misschien overkill is. Al lijkt het mij nooit slecht om in elk project een develop branch te hebben.

Link: [http://nvie.com/posts/a-successful-git-branching-model/](http://nvie.com/posts/a-successful-git-branching-model/)

## Les2: CSS animations ##
Animaties zijn zeer ineressant bij het maken van apps. Het maakt een actie honderd keer interessanter en de applicatie meteen ook een stuk boeiender.

**Goed onthouden:**

* Transitions zijn voor simpele overgangen (background-color bijvoorbeeld)
* Animations starten zichzelf / veel meer mogelijkheden als een transition. Indien een animatie op een later moment gestart moet worden hebben we JS nodig om een classe toe te voegen waardoor de animatie zal starten.

## Les3: Advanced Javascript ##
Javascript !== PHP

Als je met javascript werkt moet je de PHP logica grotendeels loslaten. Vooral de assynchrone kracht van javascript in het hoofd houden en werken met callbacks. Op die manier wordt javascript heel krachtig en is het leuk om te gebruiken.

## Les4: Building an app prototype with APIs ##
Snel een app opzetten met open data is top voor het persoonlijke portfolio, je kan snel leuke zaken bouwen.

Ook hier komt de kracht van javascript weer kijken bij het assynchroon afhandelen van requesten. Ik ben zelf grote fan van een restfull backend en een javascript front-end die assynchrone requesten doet naar die backend. 

CORS problemen oplossen met jsonp komt nog voor maar gaat in de toekomst verdwijnen. (ik denk door het registreren van de client in een Oauth systeem?). Dit is misschien nog het moeilijkste deel van het API verhaal, elke request stateless afhandelen en bepalen wie welke request doet.

## Les5: Realtime apps with node.js and web sockets ##
Gastles van d01 over node.js. Vooral gebruiken voor apps te maken met reallife data:

* Chat
* Multiplayer
* Gdocs
* Drank / toog systeem

**Onthouden packages:**

* socket.io: package voor websockets --> verbinding tussen node server en client voor het doorgeven van data
* Express: basis node server (framework?)
* Mongoose: "Entities" definiëren voor mongoDB
* node-restful: Package voor restful api routes etc.

## Les6: Angularjs ##
De grootste kracht van angular vind ik persoonlijk nog steeds de two-way-data-binding. Een angular front-end in combinatie met een restfull-api-backend heeft volgens mij zeer veel potentiel. 

Wat voor mij wel nog niet helemaal duidelijk was, is hoe de models en services op een gestructureerde manier gebouwd konden worden binnen de app. Dit was een probleem waar ik in eigen angular projecten veel heb met liggen sukkelen. De extra cursus in de paasvakantie bij Cronos was hiervoor heel nuttig en ik heb nu een methode gevonden die mij ligt. Het grote probleem bij mij was vooral nog een stuk javascript kennis dat ontbrak.

**Dir structuur**

- root
	- lib --> alle externe files 
	- src --> alle eigen files
		- Directives
		- Models --> Zijn eigenlijk gewoon constructors + de mogelijkheid om iets toe te voegen via prototype
		- Demopage1 --> view en controller van één deel samen stoppen
		- Demopage2 --> view en controller van één deel samen stoppen
		- services --> alle servies
		- app.js --> angular module, routing, ...
	- index.html --> "front-controller"

*Te bekijken: angular2 --> veel aanpassingen + veel modulairder*

## Les7: Sass + BEM ##
**Sass**

Ik werk voornamelijk graag met sass voor de mixins en de nesting maar ik zou er nog veel meer uit moeten halen. 

**In de toekomst:**

- Variabelen gebruiken voor kleuren, fonts, ...
- scss file kleiner houden door scss die bij elkaar hoort (modules) op te splitsen in specifieke files.
- Meer gebruik maken van de extend mogelijkheid --> momenteel gebruik ik dit vrijwel enkel voor een clearfix te implementeren

**BEM**

Ik geloof wel in het BEM princiepe maar toch kan ik mezelf niet overtuigen het te gebruiken. Zeker in combinatie met het nesten van classes in sass denk ik niet dat de BEM methode een voordeel opleverd. Zonder een preprocessor kan het wel zijn nut bewijzen maar dan ben ik alsnog niet helemaal overtuigd.

## Les8: Gulp ##

Tot voor kort werkte ik steeds met grunt als taskmanager. Uiteindelijk toch overgestapt op gulp omdat de piping manier zo leuk werkt. 

In standaard projecten heb ik twee taken:

- Default
	- Compile sass (via watch)
	- lint js (via watch)
	- connect (ip binding)
	- livereload (via watch)
- Build
	- minify js en css
	- concat css (via sass)
	- minify images
	- minify html

In mijn build task zou ik nog een taak moeten implementeren die alle javascript en css files concat en dan de link naar die file implementeert in mijn html files. De files samenvoegen is geen probleem maar automatisch die file inladen en de andere files uit de html halen is iets moeilijker al bestaat daar ook een handige gulp extensie voor waarmee je doormiddel van commentaar kan zeggen waar hij iets moet toevoegen.