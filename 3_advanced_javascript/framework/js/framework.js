function IMDElement(el)
{
    this.el = el;
    this.isArray = el.length > 1 ? true : false;    
}

IMDElement.prototype.css = function(prop, value)
{
    if( this.isArray )
    {
        for ( var i=0; i<this.el.length; i++)
        {
            this.el[i].style[prop] = value;
        }
    }
    else
    {
        this.el.style[prop] = value;
    }
}

function $(selector)
{
    var firstChar = selector.substring(0,1);
    switch(firstChar)
    {

        case "#":
            var whatToSelect = selector.substring(1, selector.length);
            return new IMDElement(document.getElementById(whatToSelect)); // todo-item1
            break;
            
        default:
            return new IMDElement(document.getElementsByTagName(selector));
            break;
    }
}

$("#todo1");