var express = require('express');
var router = express.Router();

/* GET /drinks/ */
router.get('/', function(req, res, next) {
  	res.render('drinks/index');

  	
});

/* GET /drinks/scoreboard */
router.get('/scoreboard', function(req, res, next) {
  res.render('drinks/scoreboard');
});

module.exports = router;
