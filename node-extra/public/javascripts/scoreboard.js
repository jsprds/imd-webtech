$(document).ready(function () {

	var socket = io.connect('localhost:3000');

	socket.on('updateDrinks', function(res){
	    if(res.drink === 'cola') {
	    	console.log('cola');

	    	$('#cola').width($('#cola').width() + 10);
	    	$('#cola').height($('#cola').height() + 10);
	    }

	    if(res.drink === 'fanta') {
	    	console.log('fanta');

	    	$('#fanta').width($('#fanta').width() + 10);
	    	$('#fanta').height($('#fanta').height() + 10);
	    }
	});

});