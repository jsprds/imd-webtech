$(document).ready(function () {

	var socket = io.connect('localhost:3000');

	var users = [];

	function getRandomColor() {
	    var letters = '0123456789ABCDEF'.split('');
	    var color = '#';
	    for (var i = 0; i < 6; i++ ) {
	        color += letters[Math.floor(Math.random() * 16)];
	    }
	    return color;
	}

	var colors = [];

	var checkUsers = function(user) {
		for(var j = 1; j < users.length; j++) {
			if(users[j].name === user) {
				return users[j];
			}
		}

		console.log(users);

		colors.push(getRandomColor());

		return users[users.length] = {
			'name': user,
			'color': colors[users.length]
		}
	}

	socket.on('initMessages', function(messages) {
		console.log('initMessages');
		console.log(messages);
		var length = messages.length;
		for(var i = 0; i < messages.length; i++) {

			var u = checkUsers(messages[i].author);



			$('#messages').append('<li><div class="panel panel-default"><div style="background-color:' + u.color + '" class="panel-heading">' + messages[i].author + '</div><div class="panel-body">' + messages[i].message + '</div></div></li>')
		}
	});

	socket.on('sendMessageCallback', function(message) {
		console.log('sendMessageCallback');
		console.log(message);
			
		var u = checkUsers(message.author);

		$('#messages').append('<li><div class="panel panel-default"><div style="background-color:' + u.color + '" class="panel-heading">' + message.author + '</div><div class="panel-body">' + message.message + '</div></div></li>')
	});

	$('#send').click(function (e) {
		e.preventDefault();
		console.log('click');
		$('#author').prop('disabled',true);
		socket.emit('sendMessage', {'author': $('#author').val(), 'message': $('#message').val()});
	});

});